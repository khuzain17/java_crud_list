package com.example.demo.Controllers;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
@RequestMapping("/no-database")
public class NoDatabaseController
{
    @RequestMapping("/hello")
    @ResponseBody
    public String Hello() {return "Hello World";}

    @Value("${spring.application.name}")
    private String name;

    @RequestMapping("/application")
    @ResponseBody
    public String getNameApp() {return name;}
}
