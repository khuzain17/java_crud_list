package com.example.demo.Controllers;

import com.example.demo.DemoApplication;
import org.slf4j.Logger; //
import org.slf4j.LoggerFactory;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class HelloWorldController {
    private static final Logger logger = LoggerFactory.getLogger(DemoApplication.class); // tambahkan import logger import org.slf4j.Logger;

    @GetMapping("/")
    public String HelloWorld(){
        return "Hello World";
    }

    @GetMapping("/Hello/{name}")
    public String HelloWorldName(@PathVariable(value = "name") String name){
        logger.info("this is part of info");
        logger.warn("this is part of warning");
        logger.error(name);
        return "Hello " + name;
    }
}
