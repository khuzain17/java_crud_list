package com.example.demo.MahasiswaNoDB;

import org.springframework.context.annotation.Bean;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.HashMap;
import java.util.Map;

@Controller
@RequestMapping("/mahasiswa/")
public class MahasiswaController {

    private static Map<String, MahasiswaObjek> mahasiswaRepo = new HashMap<>();

    //wajib static agar selalu nambah update list nya
    static {
        MahasiswaObjek mahasiswa1 = new MahasiswaObjek();
        mahasiswa1.setNama("Muhammad Khuzain");
        mahasiswa1.setNim("123");
        mahasiswaRepo.put(mahasiswa1.getNim(), mahasiswa1);

        MahasiswaObjek mahasiswa2 = new MahasiswaObjek();
        mahasiswa2.setNama("Eka Firnanda");
        mahasiswa2.setNim("323");
        mahasiswaRepo.put(mahasiswa2.getNim(), mahasiswa2);
    }

    @RequestMapping(value = "/", method = RequestMethod.GET)
    public ResponseEntity<Object> getMahasiswaObjek() {
        return new ResponseEntity<>(mahasiswaRepo.values(), HttpStatus.OK);
    }

    @RequestMapping(value = "/{id}", method = RequestMethod.DELETE)
    public ResponseEntity<Object> delele(@PathVariable("id") String id) {
        mahasiswaRepo.remove(id);
        return new ResponseEntity<>("Mahasiswa deleted succesfully", HttpStatus.OK);
    }

    @RequestMapping(value = "/{id}", method = RequestMethod.PUT)
    public ResponseEntity<Object> updateMahasiswaObjek(@PathVariable("id") String nim, @RequestBody MahasiswaObjek mahasiswaObjek) {
        mahasiswaRepo.remove(nim);
        mahasiswaObjek.setNim(nim);
        mahasiswaRepo.put(nim, mahasiswaObjek);
        return new ResponseEntity<>("Mahasiswa berhasil di update", HttpStatus.OK);
    }

    @RequestMapping(value = "/{id}", method = RequestMethod.POST)
    public ResponseEntity<Object> createMahasiswaObjek(@RequestBody MahasiswaObjek mahasiswaObjek) {
        mahasiswaRepo.put(mahasiswaObjek.getNim(), mahasiswaObjek);
        return new ResponseEntity<>("Mahasiswa berhasil di buat", HttpStatus.OK);
    }

    @Bean
    public String iniBean () {
        return ("ini Bean");
    }
}
